package com.google.ripka;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.IdTokenCredentials;
import com.google.auth.oauth2.IdTokenProvider;
import com.google.auth.oauth2.ServiceAccountCredentials;

public class SimpleSAClient {
    public static void main(String[] args) throws Exception {

        String serviceUrl = args[0];
        String serviceEndpoint = args[1];

        InputStream stream = new FileInputStream(args[2]);
        ServiceAccountCredentials cred = ServiceAccountCredentials.fromStream(stream);

        IdTokenCredentials tokenCredential = IdTokenCredentials.newBuilder()
                .setIdTokenProvider((IdTokenProvider) cred)
                .setTargetAudience(serviceUrl)
                .build();

        GenericUrl genericUrl = new GenericUrl(serviceUrl + "/" + serviceEndpoint);
        HttpCredentialsAdapter adapter = new HttpCredentialsAdapter(tokenCredential);
        HttpTransport transport = new NetHttpTransport();
        HttpRequest request = transport.createRequestFactory(adapter).buildGetRequest(genericUrl);
        HttpResponse response = request.execute();

        System.out.println(response.getStatusCode());

        BufferedReader in = new BufferedReader(new InputStreamReader(response.getContent()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();

        System.out.println(content);
    }
}