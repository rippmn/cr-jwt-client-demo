package com.google.ripka;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class GCPIdToken {

    public static String getIdToken(String jwtString) throws IOException {
        URL url = new URL("https://www.googleapis.com/oauth2/v4/token");
    
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Authorization", "Bearer " + jwtString);
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setDoOutput(true);
    
        // grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer&assertion=$JWT
        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        out.writeBytes("grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer&assertion=" + jwtString);
        out.flush();
        out.close();
    
        JsonParser parser = new JsonFactory().createParser(con.getInputStream());
    
        String idToken = null;
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            if ("id_token".equals(parser.getCurrentName())) {
                parser.nextToken();
                idToken = parser.getText();
                break;
            }
        }
    
        con.disconnect();
    
        return idToken;
    }
    
}
