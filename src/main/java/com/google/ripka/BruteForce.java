package com.google.ripka;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class BruteForce {

    public static void main(String[] args) throws Exception {

        String serviceUrl = args[0];
        String serviceEndpoint = args[1];

        ObjectMapper mapper = new ObjectMapper();
        Map<?, ?> map = mapper.readValue(new File(args[2]), Map.class);

        String jwtString = createJWT(serviceUrl, map.get("client_email").toString(), map.get("private_key").toString(),
                60000);

        String idToken = GCPIdToken.getIdToken(jwtString);

        HttpURLConnection con = (HttpURLConnection) new URL(serviceUrl + "/" + serviceEndpoint).openConnection();

        con.setRequestProperty("Authorization", "Bearer " + idToken);

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();

        con.disconnect();

        System.out.println(responseCode);
        System.out.println(content.toString());

    }

    public static String createJWT(String serviceUrl, String serviceAccount, String privateKey, long ttlMilis)
            throws JsonProcessingException, UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, SignatureException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jwtHeader = mapper.createObjectNode();

        jwtHeader.put("alg", "RS256");
        jwtHeader.put("typ", "JWT");

        String jwtHeaderStr = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jwtHeader);
        String jwtHeaderEncoded = Base64.getUrlEncoder().encodeToString(jwtHeaderStr.getBytes("UTF8"));
        long iat = System.currentTimeMillis() / 1000;
        long exp = iat + ttlMilis / 1000;

        ObjectNode jwtBody = mapper.createObjectNode();
        jwtBody.put("target_audience", serviceUrl);
        jwtBody.put("iss", serviceAccount);
        jwtBody.put("sub", serviceAccount);
        jwtBody.put("iat", iat);
        jwtBody.put("exp", exp);
        jwtBody.put("aud", "https://www.googleapis.com/oauth2/v4/token");
        String jwtBodyStr = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jwtBody);
        String jwtBodyEncoded = Base64.getUrlEncoder().encodeToString(jwtBodyStr.getBytes("UTF8"));
        byte[] jwtBytes = (jwtHeaderEncoded + "." + jwtBodyEncoded).getBytes("UTF8");

        Signature sig = Signature.getInstance("SHA256withRSA");

        String privateKeyContent = privateKey.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "");

        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey privKey = kf.generatePrivate(keySpecPKCS8);

        sig.initSign(privKey);
        sig.update(jwtBytes);
        byte[] signatureBytes = sig.sign();
        String jwtSignature = Base64.getUrlEncoder().encodeToString(signatureBytes);
        return jwtHeaderEncoded + "." + jwtBodyEncoded + "." + jwtSignature;
    }

}