This project contains Java code demonstrations of how to call the GCP endpoint to create an identity token in order to call a Cloud Run application and print the response. It is based upon this documentation [Cloud Functions Example](https://cloud.google.com/functions/docs/securing/authenticating#generating_tokens_manually) that is referenced here [Cloud Run Authenticating Service to Service](https://cloud.google.com/run/docs/authenticating/service-to-service) from the GCP documentation.

It demostrates the creation and signing of the JWT to use to request the id token using a service account via a https endpoint without the use of any GCP SDK libraries. This id then used to call the disred service where the callers must authenticate as a GCP IAM user or service account.

The Java examples provided show use of the [JJWT](https://github.com/jwtk/jjwt) open source JWT generation library as well as a brute force method using mostly Java core libraries. [Jackson Fasterxml](https://github.com/FasterXML/jackson) is used for JSON generation. 

***WARNING - This example uses a simple implementaion using a local service account json file. Service Account json files should be treated as credentials and stored in a secret management system instead of a local file. Demonstration of that scenario is beyond the scope of this demo***

**To Use this Demo**
- Create a service that returns from a simple GET Request (e.g. Hello World)
- Publish service to Cloud Run indicating `Require authentication`
![Cloud Run Publish Service Auth](doc/CR-auth.png "CR Require Authn")

- Create a Service Account and grant Cloud Run Invoker Role (or another role container roles/run.invoker permission)
- obtain a service account key for the service account and store in the root directory as `sa.json`
- get the URL from cloud run and update the examples `serviceUrl` variable.
- If there is a specific endpoint you wish to use to test change the `serviceEndpoint` variable in the exmaples to use.
- Run the examples either in IDE or using Maven as shown below (Note must supply the path to the Cloud Run Service URL, path extension to service, and path to SA json key file)
    - **BruteForce** - builds json strings of JWT and then signs using `java.security.*` classes 
        ```
        mvn clean compile exec:java -Dexec.mainClass="com.google.ripka.BruteForce" -Dexec.args="https://example-uc.a.run.app /api/hello sa.json"
        ```
    - **LibraryAssisted** - build JWT using jjwt open source project
        ```
        mvn clean compile exec:java -Dexec.mainClass="com.google.ripka.LibraryAssisted" -Dexec.args="https://example-uc.a.run.app /api/hello sa.json"
        ```
    - **SimpleSAClient** - leverage Google Auth SDK libraries to make secure call 
        ```
        mvn clean compile exec:java -Dexec.mainClass="com.google.ripka.SimpleSAClient" -Dexec.args="https://example-uc.a.run.app /api/hello sa.json"
        ```

**Other Suggested Readings**
The [Cloud Run Managing Access](https://cloud.google.com/run/docs/securing/managing-access) should be read to understand how to granularly add and remove service accounts to Cloud Run deployed services.
