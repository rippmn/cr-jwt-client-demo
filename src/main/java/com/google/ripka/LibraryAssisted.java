package com.google.ripka;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;

public class LibraryAssisted {

    public static void main(String[] args) throws Exception {

        String serviceUrl = args[0];
        String serviceEndpoint = args[1];

        ObjectMapper mapper = new ObjectMapper();
        Map<?, ?> map = mapper.readValue(new File(args[2]), Map.class);

        String jwtString = createJWT(map.get("client_email").toString(), map.get("private_key").toString(), 60000,
                serviceUrl);

        String idToken = GCPIdToken.getIdToken(jwtString);

        HttpURLConnection con = (HttpURLConnection) new URL(serviceUrl+"/"+serviceEndpoint).openConnection();

        con.setRequestProperty("Authorization", "Bearer " + idToken);

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();

        con.disconnect();

        System.out.println(responseCode);
        System.out.println(content.toString());
    }

    public static String createJWT(String saAccount, String secretKey, long ttlMillis, String targetURL)
            throws Exception {

        String privateKeyContent = secretKey.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "");

        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey privKey = kf.generatePrivate(keySpecPKCS8);

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        HashMap<String, Object> claims = new HashMap<String, Object>();
        claims.put("target_audience", targetURL);

        HashMap<String, Object> header = new HashMap<String, Object>();
        header.put("typ", "JWT");

        JwtBuilder builder = Jwts.builder().setIssuedAt(now).setSubject(saAccount).setIssuer(saAccount)
                .setAudience("https://www.googleapis.com/oauth2/v4/token").addClaims(claims)
                .signWith(privKey).setHeader(header);

        // if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }
        // Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

}